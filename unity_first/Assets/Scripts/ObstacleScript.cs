﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    public float scrollSpeed = 10.0f;
    public GameObject obstacle;
    //public GameObject scoreBox;
    public Transform obstacleSpawnPoint;
    public float minSpawnTime = 1.25f, maxSpawnTime = 1.75f;
    private float spawnTime = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        GenerateObstacle();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GameObject currentChild;
        // move all obstacle holders and delete them if oob
        for (int i = 0; i < transform.childCount; i++)
        {
            currentChild = transform.GetChild(i).gameObject;
            MoveObstacle(currentChild);
            if (currentChild.transform.position.x <= -10.0f)
            {
                Destroy(currentChild);
            }
        }
        if (Time.time >= spawnTime)
        {
            GenerateObstacle(); // spawn new obstacle after spawnTime
            spawnTime = Time.time + Random.Range(minSpawnTime, maxSpawnTime); // calculate new spawn time
        }
    }

    void MoveObstacle(GameObject currentObstacle)
    {
        currentObstacle.transform.position -= Vector3.right * (scrollSpeed * Time.deltaTime); // move obstacle to the right (actually left)
    }

    void GenerateObstacle()
    {
        // generate new obstacle holder (obstacle+score wall)
        GameObject newObstacle = Instantiate(obstacle, obstacleSpawnPoint.position, Quaternion.identity) as GameObject;
        newObstacle.transform.parent = transform;
    }

}
