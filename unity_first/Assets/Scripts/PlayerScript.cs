﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class PlayerScript : Agent
{
    public float jumpAmplification = 6.0f;
    private bool isGrounded = false;
    private Vector3 playerStartPos;
    private Quaternion playerStartRot;
    private Rigidbody playerRigidBody;
    public event Action OnReset;

    // Initialize is called before the first frame update (instead of Start())
    public override void Initialize()
    {
        // save player transformation
        playerRigidBody = transform.GetComponent<Rigidbody>();
        playerStartPos = transform.position;
        playerStartRot = transform.rotation;
    }

    // agent reaction to picked action
    public override void OnActionReceived(float[] vectorAction)
    {
        if (Mathf.FloorToInt(vectorAction[0]) == 1)
            Jump();
    }

    public override void OnEpisodeBegin()
    {
        // Reset environment
        Reset();
    }

    public override void Heuristic(float[] actionsOut)
    {
        //Player Input
        actionsOut[0] = 0;
        if (Input.GetKeyDown(KeyCode.Space)) actionsOut[0] = 1;
    }

    void Start()
    {
        //playerRigidBody = transform.GetComponent<Rigidbody>();
        //playerStartPos = transform.position;
        //playerStartRot = transform.rotation;
    }

    // Update is called once per frame, capped (to 30?) with "Fixed"
    private void FixedUpdate()
    {
        ResetXZ(); // stabalize player in X,Z -axis
        if (isGrounded)
            RequestDecision(); // check for decision when grounded (RL)
    }

    // add force to y-axis if grounded
    private void Jump()
    {
        if(isGrounded)
            playerRigidBody.AddForce(Vector3.up * jumpAmplification, ForceMode.VelocityChange);
    }

    // reset player's total transformation
    private void Reset()
    {
        transform.position = playerStartPos;
        transform.rotation = playerStartRot;
    }

    // reset player transformation in xz
    private void ResetXZ()
    {
        transform.position = new Vector3(playerStartPos.x, transform.position.y, playerStartPos.z);
        transform.rotation = playerStartRot;
    }

    // "Loose" when hitting obstacle
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            Destroy(collision.gameObject.transform.parent.gameObject); // destroy obstacle
            Reset(); // reset player location
            AddReward(increment: -1.0f); // add negative reward (RL)
            if (Score.scoreValue > Score.highScore) Score.highScore = Score.scoreValue; // check player high score
            Score.scoreValue = 0; // reset score
            EndEpisode(); // end episode (RL)
        }
    }

    // Add score when jumping over obstacle
    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Score")
        {
            AddReward(increment: 1.0f); // add reward (RL)
            Score.scoreValue += 1; // add player score
        }
    }

    // Allow player to jump when floor is hit
    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Floor")
        {
            isGrounded = true;
            ResetXZ();
        }
    }
    // Don't allow player to jump when leaving floor
    void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            isGrounded = false;
        }
    }
}
